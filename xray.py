# -*- coding: utf-8 -*-
import xlsxwriter as X
import tornado.options
import tornado.httpserver
import tornado.web
import tornado.ioloop
import datetime
import re
import os

from decimal import *

from tornado.options import define, options
define('port', default=8000, help='run on given port', type=int)

def process_xray(in_file):
    fname = in_file.split('.')[0]
    wb = X.Workbook(fname + '.xlsx')
    ws = wb.add_worksheet()
    a = []
    with open(in_file, 'r') as f:
        for x in f:
            nums = x.split(' ')
            for num in nums:
                if num != '\r\n' and num != '' and not num.isspace():
                    a.append(num)
    homopidor = Decimal(11.02)
    with open(fname + '.dat', 'w') as out:
        c = 0
        row = 0
        for n in a:
            if c > 2:
                str_homo = str(round(homopidor, 2))
                out.write(str_homo+ ' ' + n + '\n')
                ws.write(row, 0, homopidor)
                ws.write(row, 1, n)
                homopidor+= Decimal(0.02)
            else: c+=1
            row += 1
    wb.close()

class IndexHandler(tornado.web.RequestHandler):

    async def get(self):
        self.render('index.html')

    async def post(self):
        if self.request.files:
            location = await self.upload_file(self.request.files['file'][0])
        try:
            process_xray(location)
            self.redirect('r/' + location.split('.')[0].split('/')[-1])
        except Exception as e:
            print(e)



    async def upload_file(self, f):
        fname = f['filename']
        loc = 'files/' + str(fname.split('.')[0]) + '.txt'
        with open(loc, 'wb') as nf:
            nf.write(bytes(f['body']))
        return loc


class ResultHandler(tornado.web.RequestHandler):
    async def get(self, fname):
        self.render('result.html', link=fname)


class Application(tornado.web.Application):

    def __init__(self):
        handlers = [
            (r'/$', IndexHandler),
            (r'/r/(\w+)/?$', ResultHandler),
            (r'/files/(.*)/?', tornado.web.StaticFileHandler, {'path': 'files'}),
        ]

        settings = {
            'template_path': 'templates',
            'static_path': 'static',
            'xsrf_cookies': True,
            'cookie_secret': "__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        }

        tornado.web.Application.__init__(self, handlers, **settings)


def main():
    tornado.options.parse_command_line()
    application = Application()
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == '__main__':
    main()

